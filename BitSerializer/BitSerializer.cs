﻿// 
// Copyright © 2020 Timm Brooks
// 
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal 
// in the Software without restriction, including without limitation the rights 
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
// copies of the Software, and to permit persons to whom the Software is 
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in 
// all copies or substantial portions of the Software.
// 
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
// THE SOFTWARE.
// 

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace BitAssembler
{
    public class BitSerializer<T> : OrderedDictionary
    {
        /// <summary>
        /// Returns the required number of bytes to serialize.
        /// </summary>
        public int ByteLength => (int)Math.Ceiling(Values.Cast<int>().Sum() / 8d);

        /// <summary>
        /// Initializes a new instance of the <see cref="BitSerializer"/> class.
        /// </summary>
        public BitSerializer( ) { }
        /// <summary>
        /// Gets or sets the bit length with the specified field.
        /// </summary>
        /// <param name="field"></param>
        /// <returns>Length in bits of specified field</returns>
        public int this[T field] { get => (int)base[field]; set => base[field] = value; }

        /// <summary>
        /// Serializes the specified data into a byte array.
        /// </summary>
        /// <param name="data">Objects to serialize.  Parameters must be <c>byte</c>, <c>byte []</c>, or <c>int</c> variety, and number must be exactly equal to number of fields specified.</param>
        /// <returns>Serialized byte array</returns>
        public byte [] Serialize(params object [] data)
        {
            if (data.Length != Values.Count)
                throw new InvalidOperationException($"Expected {Values.Count} parameters, found {data.Length}");

            byte [] result = new byte [ByteLength];

            for (int v = 0, pos = 0; v < Values.Count; v++)
            {
                int bitLength = (int)base[v];
                byte [] bytes = GetBytes(data[v], bitLength);

                foreach (byte b in bytes)
                {
                    int remainder    = Math.Min(bitLength, 8);
                    result[pos / 8] |= (byte)((b & Mask(bitLength)) << pos % 8);
                    pos += remainder;
                    if (pos / 8 < ByteLength)
                        result[pos / 8] |= (byte)((b & Mask(bitLength)) >> remainder - pos % 8);
                    bitLength -= 8;
                    if (bitLength <= 0) break;
                }
            }
            return result;
        }
        /// <summary>
        /// Deserializes the specified data into a Dictionary.
        /// </summary>
        /// <param name="data">Data to deserialize. Must be at least <see cref="ByteLength"/> or longer.</param>
        /// <returns>Dictionary containing field values</returns>
        public Dictionary<T, byte[]> Deserialize(byte[] data)
        {
            if (data.Length < ByteLength)
                throw new ArgumentOutOfRangeException("data", "Array is not long enough to represent all required data fields.");

            Dictionary<T, byte[]> result = new Dictionary<T, byte[]>();
            int pos = 0;
            foreach (T field in Keys)
            {
                int bitLength = (int)base[field];
                byte [] bytes = new byte [bitLength / 8 + 1];

                for (int b = 0; b < bytes.Length; b++)
                {
                    int remainder = Math.Min(bitLength, 8);
                    bytes[b] |= (byte)((data[pos / 8] >> pos % 8) & Mask(remainder));
                    pos += remainder;
                    if (pos / 8 < ByteLength)
                        bytes[b] |= (byte)((data[pos / 8] << remainder - pos % 8) & Mask(bitLength));
                    bitLength -= 8;
                }

                result.Add(field, bytes);
            }

            return result;
        }

        private static byte Mask(int bitLength) => (byte)(Math.Pow(2, bitLength) - 1);
        private static byte [] GetBytes(object value, int bitLength)
        {
            byte [] result;
            switch (value)
            {
                case byte[] array: result  = array; break;
                case byte b: result        = BitConverter.GetBytes(b); break;
                case short int16: result   = BitConverter.GetBytes(int16); break;
                case int int32: result     = BitConverter.GetBytes(int32); break;
                case long int64: result    = BitConverter.GetBytes(int64); break;
                case ushort uint16: result = BitConverter.GetBytes(uint16); break;
                case uint uint32: result   = BitConverter.GetBytes(uint32); break;
                case ulong uint64: result  = BitConverter.GetBytes(uint64); break;
                default: throw new ArgumentException("Only types byte, byte [], short, int, long, ushort, uint, ulong arguments permitted");
            }

            Array.Resize(ref result, (int)Math.Ceiling(bitLength / 8d));
            return result;
        }
    }
}
