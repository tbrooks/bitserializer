﻿// 
// Copyright © 2020 Timm Brooks
// 
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal 
// in the Software without restriction, including without limitation the rights 
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
// copies of the Software, and to permit persons to whom the Software is 
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in 
// all copies or substantial portions of the Software.
// 
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
// THE SOFTWARE.
// 

using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Collections.Generic;

namespace BitAssembler
{
    [TestClass]
    public class BitAssemblerTest
    {
        enum TestFields { Version, Data, Expiration, CRC, Test }
        byte version    = 0x01;
        byte data       = 0xff;
        byte expiration = 0x99;
        byte crc        = 0x3a;

        [TestMethod]
        public void ByteLength()
        {
            BitSerializer<TestFields> ba = new BitSerializer<TestFields>()
            {
                [TestFields.Version]    = 3,
                [TestFields.Data]       = 11,
                [TestFields.Expiration] = 5,
                [TestFields.CRC]        = 4,
                [TestFields.Test]       = 9
            };

            Assert.AreEqual(4, ba.ByteLength);
        }
        [TestMethod]
        public void SerializeSingleByte()
        {
            byte [] expected = new byte [] { version };   
            BitSerializer<TestFields> ba = new BitSerializer<TestFields>()
            {
                [TestFields.Version]    = 8
            };

            byte [] actual = ba.Serialize(version);

            CollectionAssert.AreEqual(expected, actual,
                $"\r\n\texpected: {BitConverter.ToString(expected).Replace("-", "")}\r\n\tactual: {BitConverter.ToString(actual).Replace("-", "")}\r\n");
        }
        [TestMethod]
        public void SerializeMultipleBytes()
        {
            byte [] expected = new byte [] { version, data, expiration, crc };   
            BitSerializer<TestFields> ba = new BitSerializer<TestFields>()
            {
                [TestFields.Version]    = 8,
                [TestFields.Data]       = 8,
                [TestFields.Expiration] = 8,
                [TestFields.CRC]        = 8
            };

            byte [] actual = ba.Serialize(version, data, expiration, crc);

            CollectionAssert.AreEqual(expected, actual,
                $"\r\n\texpected: {BitConverter.ToString(expected).Replace("-", "")}\r\n\tactual: {BitConverter.ToString(actual).Replace("-", "")}\r\n");
        }
        [TestMethod]
        public void SerializePartialByte()
        {
            version          = 0xff;
            byte [] expected = new byte[] { 0x0f };
            BitSerializer<TestFields> ba = new BitSerializer<TestFields>()
            {
                [TestFields.Version] = 4
            };

            byte [] actual = ba.Serialize(version);

            CollectionAssert.AreEqual(expected, actual,
                $"\r\n\texpected: {BitConverter.ToString(expected).Replace("-", "")}\r\n\tactual: {BitConverter.ToString(actual).Replace("-", "")}\r\n");
        }
        [TestMethod]
        public void SerializeFromArray()
        {
            byte [] data     = new byte [] { 0x0f, 0x0e };
            byte [] expected = new byte [] { 0x0f, 0x0e };
            BitSerializer<TestFields> ba = new BitSerializer<TestFields>()
            {
                [TestFields.Version] = 16
            };

            byte [] actual = ba.Serialize(data);

            CollectionAssert.AreEqual(expected, actual,
                $"\r\n\texpected: {BitConverter.ToString(expected).Replace("-", "")}\r\n\tactual: {BitConverter.ToString(actual).Replace("-", "")}\r\n");
        }
        [TestMethod]
        public void SerializeFromByteAndArray()
        {
            byte [] version      = new byte [] { 0b0000_0001, 0b1111_1000 };
            byte [] data         = new byte [] { 0b0000_0001, 0b1111_1110 };
            const int expiration = 0b0010_0001;
            const int crc        = 0b1001;
            byte [] expected     = new byte [] { 0b0000_0001, 0b0001_1000, 0b0110_0000, 0b1001_1000 };
            BitSerializer<TestFields> ba = new BitSerializer<TestFields>()
            {
                [TestFields.Version]    = 12,
                [TestFields.Data]       = 10,
                [TestFields.Expiration] = 6,
                [TestFields.CRC]        = 4
            };

            byte [] actual = ba.Serialize(version, data, expiration, crc);

            CollectionAssert.AreEqual(expected, actual,
                $"\r\n\texpected: {BitConverter.ToString(expected).Replace("-", "")}\r\n\tactual: {BitConverter.ToString(actual).Replace("-", "")}\r\n");
        }
        [TestMethod]
        public void SerializeTruncatedArray()
        {
            byte [] expected = new byte [] { 0b0000_1101, 0b0100_0000, 0b1100_1100, 0b1000_0000 };
            BitSerializer<TestFields> ba = new BitSerializer<TestFields>()
            {
                [TestFields.Version]    = 3,
                [TestFields.Data]       = 11,
                [TestFields.Expiration] = 5,
                [TestFields.CRC]        = 4,
                [TestFields.Test]       = 9
            };

            byte [] result = ba.Serialize(5, new byte [] { 0x01 }, 17, 9, 257);

            CollectionAssert.AreEqual(expected, result);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void DeserializeTruncatedArray()
        {
            byte [] original     = new byte [] { 0b0000_0001, 0b0001_1000 };
            BitSerializer<TestFields> ba = new BitSerializer<TestFields>()
            {
                [TestFields.Version]    = 12,
                [TestFields.Data]       = 10,
                [TestFields.Expiration] = 6,
                [TestFields.CRC]        = 4
            };

            Dictionary<TestFields, byte []> result = ba.Deserialize(original);
        }
        [TestMethod]
        public void RoundTrip()
        {
            BitSerializer<TestFields> ba = new BitSerializer<TestFields>()
            {
                [TestFields.Version] = 3,
                [TestFields.Data] = 11,
                [TestFields.Expiration] = 5,
                [TestFields.CRC] = 4,
                [TestFields.Test] = 9
            };

            Dictionary<TestFields, byte[]> result = ba.Deserialize(ba.Serialize(5, 1025, 17, 9, 257));

            Assert.AreEqual(5, result[TestFields.Version][0]);
            Assert.AreEqual(1025, BitConverter.ToInt16(result[TestFields.Data], 0));
            Assert.AreEqual(17, result[TestFields.Expiration][0]);
            Assert.AreEqual(9, result[TestFields.CRC][0]);
            Assert.AreEqual(257, BitConverter.ToInt16(result[TestFields.Test], 0));
        }
    }
}
